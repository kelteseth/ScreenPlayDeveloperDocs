<div>
<img width="100%" height="93" src="logo_gitlab_fullwidth.svg">
</div>


<div align="center">

<br>

ScreenPlay [![pipeline status](https://gitlab.com/kelteseth/ScreenPlay/badges/master/pipeline.svg)](https://gitlab.com/kelteseth/ScreenPlay/-/commits/master) <br>
Dev. Docs   [![pipeline status](https://gitlab.com/kelteseth/ScreenPlayDeveloperDocs/badges/master/pipeline.svg)](https://gitlab.com/kelteseth/ScreenPlayDeveloperDocs/-/commits/master) <br><br>
<a href=""></a>
[中文总览](README_zh_CN.md)
<br><br>
<a href="https://twitter.com/kelteseth">![Twitter Follow](https://img.shields.io/twitter/follow/kelteseth?style=for-the-badge)</a>
<a href="https://www.reddit.com/r/ScreenPlayApp/">![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/screenplayapp?style=for-the-badge)</a>
<br><br>
User Chat (Discord)
<a href="https://discord.com/invite/JBMmZbgc?utm_source=Discord%20Widget&utm_medium=Connect">![Discord](https://img.shields.io/discord/516635043435773970?style=for-the-badge)</a>
Developer Chat [![Gitter](https://badges.gitter.im/ScreenPlayApp/community.svg)](https://gitter.im/ScreenPlayApp/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)



<br>
ScreenPlay is an open source cross platform app for displaying Video Wallpaper, Widgets and AppDrawer. It is written in modern C++20/Qt5/QML. Binaries with workshop support are available for Windows and soon Linux & MacOSX via <a href="https://store.steampowered.com/app/672870/ScreenPlay/">Steam</a>.  Join our community: <a href="https://screen-play.app/">Homepage</a> - <a href="https://forum.screen-play.app/">Forum</a>

<br> 
<h3><a href="https://store.steampowered.com/app/672870/ScreenPlay/">✨Download ScreenPlay via Steam</a> - <a href="https://steamcommunity.com/app/672870/workshop/">Steam Workshop for Wallpaper and Widgets✨</a></h3> 
Windows only, Linux and MacOS (soon™)
<br>
<br>

<a href="https://www.youtube.com/watch?v=q-J2fTWDxw8">

# About 

This repo takes the latest ScreenPlay and compiles the documentation inside all the cpp files. Available at https://kelteseth.gitlab.io/ScreenPlayDeveloperDocs/
